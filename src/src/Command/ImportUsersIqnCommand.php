<?php
/* @author Jorge
 * @date 27/02/2019
 * @version 1.0
 * @copyright Prometeo
 *
 * This command imports IQN users from your REST API sharepoint server and stores them in Hermes
 * through the endpoint /api/xxxxx/
 */
//TODO Sacar parametros a un .INI
//TODO Cuando proceda sincronizar a la inversa
//TODO Refactorizar syncUser()
//TODO Sistema de logs
//TODO Revisar todos sin fecha limite por parametro de cmd
//TODO Refactorizar nombres para generico

namespace App\Command;

use HttpException;
use Psr\Log\InvalidArgumentException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use UnexpectedValueException;

/**
 * Class ImportUsersIqnCommand
 * @package App\Command
 */
class ImportUsersIqnCommand extends Command
{
    /**
     * Set the id of the entity
     */
    const ENTIDAD = 39;
    /**
     * Disable users
     */
    const API_HERMES_DISABLE_USERS = "https://produccion.mecitas.prometeoinnovations.com/app_dev.php/api/v3/propietarioContenido/desactivar/%%id%%";
    /**
     * Set ENDPOINT to get Hermes users
     */
    const API_HERMES_GET_USERS = "https://produccion.mecitas.prometeoinnovations.com/app_dev.php/api/v3/propietarioContenido/entidad/%%entidad%%";
    /**
     * Set ENDPOINT to create Hermes users
     */
    const API_HERMES_CREATE_USERS = 'https://produccion.mecitas.prometeoinnovations.com/app_dev.php/api/v3/propietarioContenido/';
    /**
     * Set ENDPOINT to update Hermes users
     */
    const API_HERMES_UPDATE_USERS = 'https://produccion.mecitas.prometeoinnovations.com/app_dev.php/api/v3/propietarioContenido/%%id%%';
    /**
     * Set the endpoint to get users on sharepoint
     */
    const API_SHAREPOINT_GET_USERS = 'https://sharepoint.quasi.nalonchem.com/_api/web/lists/GetByTitle%28%27Lista%20de%20Empleados%27%29/Items?$skiptoken=Paged=FALSE&$top=1000&$select=Nombre/Title,DNI,Modified,Id,FechaIncorporacion,FechaBaja,Matricula&$expand=Nombre/Title';
    /**
     * Establishes where to get users in the hierarchy of Sharepoint's ENDPOINT response.
     * The returned array is where it is iterated to get the users.
     */
    const START_API = "d.results";
    /**
     * Defines the attribute mapping between the source endpoint and Hermes
     */
    const MAPPING_API = [
        "nombre" => "Nombre.Title",
        "fechaAlta" => "FechaIncorporacion",
        "dni" => "DNI",
        "fechaModificacion" => "Modified"
    ];
    /**
     * Defines the comparison key between two ENDPOINTs
     */
    const KEY_API = "dni";
    /**
     * Defines the comparison key between the two ENDPOINTs
     */
    const UPDATE_AT_PROPERTY = "Modified";
    /**
     * Defines how to convert ENDPOINT dates
     */
    const UPDATE_AT_REGEX = "/(\d+)-(\d+)-(\d+)T(\d+):(\d+):(\d+)Z/";
    const UPDATE_AT_REPLACE = "$1-$2-$3 $4:$5:$6";
    const APP_CACHE_LASTRUN_LOG = "/var/cache/lastrun.log";
    /** Set default values */
    const DEFAULT_VALUES = [
        "activo" => true,
        "validated" => true,
        "recibirNotificacionesPush" => true,
    ];
    /**
     * @var string
     */
    protected static $defaultName = 'import:users:iqn';
    /** @var */
    protected $container;

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //Get container

        $this->container = $this->getApplication()->getKernel()->getContainer();
        //Get modified Sharepoint user from ENDPOINT
        $allSharepointUsers = $this->getSharepointUsers();
        if (empty($allSharepointUsers)) {
            throw new UnexpectedValueException("No se han encontrados usuarios de Sharepoint");
        }//if
        list ($modifiedSharepointUsers, $allSharepointIdUsers) = $this->mappingEndpoint2Hermes($allSharepointUsers, self::START_API, self::MAPPING_API);
        //Get modified Hermes users
        list ($modifiedHermesUsers, $allHermesIdUsers) = $this->getHermesUsers(self::ENTIDAD);
        if (empty($modifiedHermesUsers)) {
            throw new UnexpectedValueException("No se han encontrados usuarios de Hermes");
        }//if
        //Sync Users
        $this->syncUsers($modifiedHermesUsers, $modifiedSharepointUsers, $allHermesIdUsers, $allSharepointIdUsers);
    }//protected

    /**
     * Get users and modification dates from the Sharepoint server
     *
     * @return mixed|null
     * @throws \Exception
     */
    protected function getSharepointUsers()
    {
        //Get users from Endpoint
        $response = $this->request(
            self::API_SHAREPOINT_GET_USERS,
            "GET",
            [
                "Accept: application/json; odata=verbose",
                'cache-control: no-cache',
                'Authorization: Basic cXVhc2lcQXBwRW1wbGVhZG86amRzWGE4Z0U4YXBUNUZ1TGdsSmw='
            ]
        );
        //Decode and return response
        try {
            return !empty($response) ? json_decode($response, true) : null;
        }//try
        catch (InvalidArgumentException $e) {
            throw new Exception("Json Inválido");
        }//catch
    }//protected

    /**
     * Establishes a connection to an ENDPOINT API REST
     *
     * @param $url
     * @param $method
     * @param null $data
     * @param $headers
     * @return mixed
     * @throws \Exception
     */
    protected function request($url, $method, $headers, $data = null)
    {
        //Ckecks vars
        if (!is_string($url) || !preg_match("/^https*:\/\//", $url)) {
            throw new \InvalidArgumentException("ENDPOINT no válido");
        }//if
        if (!in_array($method, ["GET", "POST", "PUT", "DELETE", "PATCH"])) {
            throw new \InvalidArgumentException("METHOD no válido");
        }//if
        //Connect
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => ($data) ? http_build_query($data) : "",
            CURLOPT_HTTPHEADER => $headers
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        //Check errors
        if ($err) {
            throw new \HttpException("$err con $url");
        }//if
        //Return response
        return $response;
    }//protected

    /**
     * map endpoint attributes with Hermes from established definition
     *
     * @param $origin
     * @param $start
     * @param $mapping
     * @return array|null
     */
    protected function mappingEndpoint2Hermes($origin, $start, $mapping)
    {
        //Initialize vars
        $startArray = [];
        $destination = [];
        //Check point of entry
        try {
            eval ("\$startArray = \$origin['" . preg_replace("/\./", "']['", $start) . "'];");
            if (!$startArray || !$mapping) {
                return null;
            }//if
        }//try
        catch (InvalidArgumentException $e) {
            throw new InvalidArgumentException("Punto de entrada incorrecto");
        }//catch
        //Attribute mapping
        $index = 0;
        foreach ($startArray as $item) {
            foreach ($mapping as $key => $attribute) {
                try {
                    eval ("\$destination[$index]['$key'] = \$item['" . preg_replace("/\./", "']['", $attribute) . "'];");
                }//try
                catch (InvalidArgumentException $e) {
                    throw new InvalidArgumentException("Mapping incorrecto");
                }//catch
            }//foreach
            $index++;
        }//foreach
        //Check mapping
        if (!$destination) {
            return null;
        }//If
        // Change the key of the array to the defined attribute and change the set date of modification.
        foreach ($destination as $key => $user) {
            $users_id[] = strtolower($user[self::KEY_API]);

            if (!empty($user[self::KEY_API])) {
                $destination[strtolower($user[self::KEY_API])] = $user;
                unset($destination[$key]);
            }//if
        }//foreach
        //Return response
        return [$destination, $users_id];
    }//protected

     /**
     * Get the last time the update script was launched
     *
     * @return int
     */
    protected function getLastRun()
    {
        //Set root dir
        $root = $this->container->get('kernel')->getRootDir() . "/..";
        $content = @file_get_contents($root . self::APP_CACHE_LASTRUN_LOG);
        //Return response
        $lastRun = json_decode($content, true);
        return $lastRun && !empty($lastRun[__CLASS__]) ? $lastRun[__CLASS__] : -1;
    }//protected

    /**
     * Get users from the Hermes server
     *
     * @param $entidad
     * @return array|null
     * @throws \Exception
     */
    protected function getHermesUsers($entidad)
    {
        //Get Users from endpoint
        $url = preg_replace("/%%entidad%%/", $entidad, self::API_HERMES_GET_USERS);
        $response = $this->request(
            $url,
            "GET",
            [
                "Accept: application/json",
                'cache-control: no-cache',
            ]
        );
        //Check response
        if (empty($response)) {
            return null;
        }//if

        try {
            //Decode and check response
            $users = json_decode($response, true);
            if (!$users) {
                return null;
            }//if
            // Change the key of the array to the defined attribute 
            foreach ($users as $key => $user) {
                $users_id[$user["id"]] = strtolower($user[self::KEY_API]);

                if (!empty($user[self::KEY_API])) {
                    $users[strtolower($user[self::KEY_API])] = $user;
                    unset($users[$key]);
                }//if
            }//foreach
        }//try
        catch (InvalidArgumentException $e) {
            throw new Exception("Json Inválido");
        }//catch
        //Return response
        return [$users, $users_id];
    }//protected

    /**
     * synchronizes the two servers using for it the intersection and subtraction of sets
     *
     * @param $modifiedHermesUsers
     * @param $modifiedSharepointUsers
     * @param $allHermesIdUsers
     * @param $allHermesIdSharepoint
     * @throws \Exception
     */
    protected function syncUsers($modifiedHermesUsers, $modifiedSharepointUsers, $allHermesIdUsers, $allHermesIdSharepoint)
    {
        //calculation of the modifications in each endpoint by theory of sets
        $modifiedInBoth = array_intersect_key($modifiedSharepointUsers, $modifiedHermesUsers);
        $modifiedOnlyInHermes = array_diff_key($modifiedHermesUsers, $modifiedSharepointUsers);
        $modifiedOnlyInSharepoint = array_diff_key($modifiedSharepointUsers, $modifiedHermesUsers);
        // Hermes is updated with the modifications made in both servers and those made in Sharepoint
        if ($changesForHermes = array_merge($modifiedInBoth, $modifiedOnlyInSharepoint)) {
            foreach ($changesForHermes as $item) {
                $fechaModificacion=preg_replace(self::UPDATE_AT_REGEX, self::UPDATE_AT_REPLACE, $item["fechaModificacion"]);
                $fechaModificacion =\DateTime::createFromFormat("Y-m-d H:i:s", $fechaModificacion );
                $ts = $fechaModificacion? $fechaModificacion-> getTimestamp():0;
                if ($ts < $this->getLastRun()){
                    echo "Descarto (no actualizado) usuario con ".self::KEY_API." ". $item[self::KEY_API] . PHP_EOL;
                    continue;
                }//if

                //attribute adjustment
                if (empty($item["apellidos"])) {
                    list($item["nombre"], $item["apellidos"]) = $this->splitSurnameAndName($item["nombre"]);
                }//if
                if (empty($item["alias"])) {
                    $item["alias"] = $this->createAlias($item["nombre"], $item["apellidos"]);
                }//if
                if (!empty($item["fechaModificacion"])) {
                    unset($item["fechaModificacion"]);
                }//If
                if (!empty($item["fechaAlta"])) {
                    unset($item["fechaAlta"]);
                }//If
                if (empty($item["entidad"])) {
                    $item["entidad"] = self::ENTIDAD;
                }//If

                // if the element exists is a modification
                if (in_array(strtolower($item[self::KEY_API]), $allHermesIdUsers)) {
                    echo "Actualizando usuario con ".self::KEY_API." ". $item[self::KEY_API] . PHP_EOL;
                    $url = preg_replace(
                        "/%%id%%/", array_search(strtolower($item[self::KEY_API]),$allHermesIdUsers),
                        self::API_HERMES_UPDATE_USERS
                    );
                    $response = $this->request(
                        $url,
                        "PATCH",
                        ["Content-Type: application/x-www-form-urlencoded"],
                        $item
                    );
                    var_dump($response);
                }
                // if the element exists is a modification
                else {
                    echo "Creando usuario con ".self::KEY_API." ". $item[self::KEY_API] . PHP_EOL;
                    //Set Default Values
                    if (self::DEFAULT_VALUES) foreach (self::DEFAULT_VALUES as $key=>$value){
                        if (empty($item[$key])) {
                            $item[$key]=$value;
                        }//if
                    }//foreach
                    // Connect
                    $response = $this->request(
                        self::API_HERMES_CREATE_USERS,
                        "POST",
                        ["Content-Type: application/x-www-form-urlencoded"],
                        $item
                    );
                    var_dump($response);
                }//else
            }//foreach
        }//if
        //the modified elements in Hermes, nothing is done with them
        if ($modifiedOnlyInHermes) {
            foreach ($modifiedOnlyInHermes as $item) {
                //TODO DE MOMENTO NO ACTUALIZAMOS EN ESTE SENTIDO
            }//foreach
        }//if
        // deactivate users that only exist in Hermes
        if ($onlyExistInHermes = array_diff($allHermesIdUsers, $allHermesIdSharepoint)) {
            foreach ($onlyExistInHermes as $key => $user) {
                echo "Desactivar " . $key . PHP_EOL;
                $url = preg_replace("/%%id%%/", $key, self::API_HERMES_DISABLE_USERS);
                $response = $this->request(
                    $url,
                    "PATCH",
                    ["Content-Type: application/x-www-form-urlencoded"]
                );
                var_dump($response);
            }//foreach
        }//if
        // update date of last synchronization

        $this->setLastRun();
    }//function

    /**
     * when there are no surnames tries to separate the first name into two parts
     *
     * @param $nombre
     * @return array
     */
    protected function splitSurnameAndName($nombre)
    {
        if (!$nombre) {
            return [null, null];
        }//if
        $palabras = explode(" ", $nombre);

        if (count($palabras) == 1) {
            return [$nombre, null];
        }//if

        if (count($palabras) == 2) {
            return [$palabras[0], $palabras[1]];
        }//if

        $nombre = "";
        $apellidos = "";
        if (count($palabras) >= 2) {
            for ($i = 0; $i < count($palabras) - 2; $i++) {
                $nombre .= $palabras[$i] . " ";
            }//for

            for (count($palabras) - 1; $i < count($palabras); $i++) {
                $apellidos .= $palabras[$i] . " ";
            }//for

            return [trim($nombre), trim($apellidos)];
        }//if
    }//function


    protected function normaliza($cadena)
    {
        $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        $cadena = utf8_decode($cadena);
        $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
        $cadena = strtolower($cadena);
        return utf8_encode($cadena);
    }

    /**
     * creates an alias from the initials of names and surnames
     *
     * @param $nombre
     * @param $apellidos
     * @return string|null
     */
    protected function createAlias($nombre, $apellidos)
    {
        $nombre = $this->normaliza($nombre);
        $apellidos = $this->normaliza($apellidos);
        $aliasStr = $nombre . " " . $apellidos;
        $aliasStr = preg_replace(["/ de /", "/ por /", "/ para /", "/ el /", "/ la /", "/ lo /"], " ", $aliasStr);
        $aliasPalabras = explode(" ", $aliasStr);

        if (!$aliasPalabras) {
            return null;
        }//if

        $alias = "";
        foreach ($aliasPalabras as $palabra) {
            $alias .= ucfirst(substr($palabra, 0, 1));
        }//foreach

        return $alias;
    }//function

    /**
     * Get the last time the update script was launched
     *
     * @return void
     */
    protected function setLastRun()
    {
        $root = $this->container->get('kernel')->getRootDir() . "/..";
        $content = @file_get_contents($root . self::APP_CACHE_LASTRUN_LOG);

        $lastRun = json_decode($content, true);
        if (!$lastRun) {
            $lastRun = [];
        }//if
        $lastrun [__CLASS__] = (new \DateTime())->getTimeStamp();

        file_put_contents($root . self::APP_CACHE_LASTRUN_LOG, json_encode($lastrun));
    }//function

}//class